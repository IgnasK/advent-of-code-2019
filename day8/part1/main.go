package main

import "fmt"

type Layer [][]int
type Image struct {
	layers []Layer
}

func (l Layer) Distribution() map[int]int {
	dist := make(map[int]int)
	for _, r := range l {
		for _, v := range r {
			dist[v] += 1
		}
	}

	return dist
}

func arrangeLayers(width, height int, values []int) Image {

	if len(values) % (width * height) != 0 {
		panic(fmt.Sprintf("Read %d numbers, which is not a multiple of %d*%d", len(values), width, height))
	}

	layers := make([]Layer, 0)
	for l := 0; l < len(values) / (width * height); l += 1 {
		rows := make(Layer, height)

		for i := 0; i < height; i += 1 {
			startIndex := l * (width * height) + i * width
			endIndex := startIndex + width
			rows[i] = values[startIndex:endIndex]
		}

		layers = append(layers, rows)
	}

	return Image{
		layers,
	}
}

func read() Image {
	var width, height int
	if _, err := fmt.Scanf("%d %d", &width, &height); err != nil {
		panic(err)
	}

	values := make([]int, 0)

	for {
		var v rune
		if _, err := fmt.Scanf("%c", &v); err != nil {
			break
		}

		value := int(v) - '0'
		if value < 0 || value > 9 {
			// Only interested in digits and not whitespace
			continue
		}

		values = append(values, value)
	}

	return arrangeLayers(width, height, values)
}

func main() {
	image := read()

	lowest0 := 100000
	result := 0

	for _, layer := range image.layers {
		dist := layer.Distribution()

		if dist[0] < lowest0 {
			lowest0 = dist[0]
			result = dist[1] * dist[2]
		}
	}

	fmt.Println(lowest0, result)
}