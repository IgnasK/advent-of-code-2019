package main

import "fmt"

type Layer [][]int
type Image struct {
	layers []Layer
}

func (l Layer) Print() {
	for _, row := range l {
		for _, v := range row {
			if v == 0 {
				fmt.Printf("%c", '\u2B1B')
			} else if v == 1 {
				fmt.Printf("%c", '\u2B1C')
			} else {
				fmt.Print(' ')
			}
		}
		fmt.Println()
	}
}

func (l Layer) Distribution() map[int]int {
	dist := make(map[int]int)
	for _, r := range l {
		for _, v := range r {
			dist[v] += 1
		}
	}

	return dist
}

func (image Image) Squash() Layer {
	layer := make(Layer, len(image.layers[0]))

	for i, row := range image.layers[0] {
		layer[i] = make([]int, len(row))
	}

	for i, row := range layer {
		for j := range row {
			for _, l := range image.layers {
				layerValue := l[i][j]
				layer[i][j] = layerValue
				if layerValue != 2 {
					break
				}
			}
		}
	}

	return layer
}

func arrangeLayers(width, height int, values []int) Image {

	if len(values) % (width * height) != 0 {
		panic(fmt.Sprintf("Read %d numbers, which is not a multiple of %d*%d", len(values), width, height))
	}

	layers := make([]Layer, 0)
	for l := 0; l < len(values) / (width * height); l += 1 {
		rows := make(Layer, height)

		for i := 0; i < height; i += 1 {
			startIndex := l * (width * height) + i * width
			endIndex := startIndex + width
			rows[i] = values[startIndex:endIndex]
		}

		layers = append(layers, rows)
	}

	return Image{
		layers,
	}
}

func read() Image {
	var width, height int
	if _, err := fmt.Scanf("%d %d", &width, &height); err != nil {
		panic(err)
	}

	values := make([]int, 0)

	for {
		var v rune
		if _, err := fmt.Scanf("%c", &v); err != nil {
			break
		}

		value := int(v) - '0'
		if value < 0 || value > 9 {
			// Only interested in digits and not whitespace
			continue
		}

		values = append(values, value)
	}

	return arrangeLayers(width, height, values)
}

func main() {
	image := read()

	layer := image.Squash()

	layer.Print()
}