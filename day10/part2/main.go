package main

import (
	"bufio"
	"fmt"
	"os"
	"sort"
)

type Position struct {
	x, y int
}

func (p Position) Sub(o Position) Direction {
	return Direction{
		p.x - o.x,
		p.y - o.y,
	}
}

type Direction Position

func (d Direction) Quadrant() int {
	if d.x >= 0 && d.y <= 0 {
		return 0
	}
	if d.x >= 0 && d.y >= 0 {
		return 1
	}
	if d.x <= 0 && d.y >= 0 {
		return 2
	}
	if d.x <= 0 && d.y <= 0 {
		return 3
	}

	// Should never reach
	return 0
}

func Abs(x int) int {
	if x > 0 {
		return x
	} else {
		return -x
	}
}

func (d Direction) Length() int {
	return Abs(d.x) + Abs(d.y)
}

func (d Direction) Normalise() Direction {
	gcd := GCD(Abs(d.x), Abs(d.y))
	return Direction{
		d.x / gcd,
		d.y / gcd,
	}
}

func (d Direction) Rotate() Direction {
	return Direction{
		-d.y,
		d.x,
	}
}

type Dimensions struct {
	width, height int
}

func GCD(x, y int) int {
	if y == 0 {
		if x == 0 {
			return 1
		}
		return x
	}

	return GCD(y, x%y)
}

func Angle(origin, point Position) (Direction, int) {
	dir := point.Sub(origin)
	length := dir.Length()

	return dir.Normalise(), length
}

func read() (Dimensions, []Position) {
	var width, height int

	positions := make([]Position, 0)

	scanner := bufio.NewScanner(os.Stdin)
	for scanner.Scan() {
		line := scanner.Text()
		width = len(line)
		height++

		for x, v := range line {
			if v == '#' {
				positions = append(positions, Position{x, height - 1})
			}
		}
	}

	return Dimensions{width, height}, positions
}

func reachable(origin Position, asteroids []Position) map[Direction]Position {
	directions := make(map[Direction]Position)
	lengths := make(map[Direction]int)

	for _, a := range asteroids {
		dir, length := Angle(origin, a)
		if _, exists := directions[dir]; exists {
			if lengths[dir] < length {
				continue
			}
		}
		directions[dir] = a
		lengths[dir] = length
	}

	return directions
}

func SortPositions(origin Position, asteroids []Position) {
	sort.Slice(asteroids, func(i, j int) bool {
		angleI, _ := Angle(origin, asteroids[i])
		angleJ, _ := Angle(origin, asteroids[j])

		qI := angleI.Quadrant()
		qJ := angleJ.Quadrant()

		if qI != qJ {
			return qI < qJ
		}

		return angleI.x*angleJ.y-angleJ.x*angleI.y > 0
	})
}

func reachableOrder(origin Position, asteroids []Position) []Position {
	remainingAsteroids := make(map[Position]bool)

	for _, a := range asteroids {
		remainingAsteroids[a] = true
	}

	delete(remainingAsteroids, origin)

	order := make([]Position, 0)

	for len(remainingAsteroids) > 0 {
		subset := make([]Position, 0)
		for a := range remainingAsteroids {
			subset = append(subset, a)
		}

		r := reachable(origin, subset)

		reachableSubset := make([]Position, 0)
		for _, a := range r {
			reachableSubset = append(reachableSubset, a)
			delete(remainingAsteroids, a)
		}

		SortPositions(origin, reachableSubset)

		order = append(order, reachableSubset...)
	}

	return order
}

func countReachable(origin Position, asteroids []Position) int {
	directions := reachable(origin, asteroids)

	return len(directions)
}

func main() {
	_, asteroids := read()

	bestReachable := 0
	var bestPosition Position

	for _, pos := range asteroids {
		reachable := countReachable(pos, asteroids)
		if reachable > bestReachable {
			bestReachable = reachable
			bestPosition = pos
		}
	}

	order := reachableOrder(bestPosition, asteroids)

	result := order[199].x*100 + order[199].y
	fmt.Println(result)
}
