package main

import "testing"

import "fmt"

func assertPos(v, expected Position, message string, t *testing.T) {
	if v != expected {
		t.Errorf("Got %v, expected %v for %s", v, expected, message)
	}
}

func TestSort(t *testing.T) {
	expected := []Position{
		{0, -1},
		{1, -4},
		{1, -3},
		{2, -5},
		{2, -2},
		{1, 0},
		{0, 1},
		{-1, 0},
	}

	positions := []Position{
		{2, -2},
		{1, -4},
		{1, 0},
		{1, -3},
		{0, -1},
		{2, -5},
		{0, 1},
		{-1, 0},
	}

	SortPositions(Position{0, 0}, positions)

	for i := range expected {
		assertPos(positions[i], expected[i], fmt.Sprintf("position %d", i), t)
	}
}
