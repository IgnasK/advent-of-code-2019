package main

import "testing"

func assertInt(v, expected int, message string, t *testing.T) {
	if v != expected {
		t.Errorf("Got %d, expected %d for %s", v, expected, message)
	}
}

func TestNormalise(t *testing.T) {

	dir := Direction{10, 10}

	norm := dir.Normalise()

	assertInt(norm.x, 1, "x", t)
	assertInt(norm.y, 1, "y", t)
}

func TestNormalise2(t *testing.T) {

	dir := Direction{3, 6}

	norm := dir.Normalise()

	assertInt(norm.x, 1, "x", t)
	assertInt(norm.y, 2, "y", t)

}

func TestNormaliseNegative(t *testing.T) {

	dir := Direction{-3, -12}

	norm := dir.Normalise()

	assertInt(norm.x, -1, "x", t)
	assertInt(norm.y, -4, "y", t)

}
