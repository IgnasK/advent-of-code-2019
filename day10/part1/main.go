package main

import (
	"bufio"
	"fmt"
	"os"
)

type Position struct {
	x, y int
}

func (p Position) Sub(o Position) Direction {
	return Direction{
		p.x - o.x,
		p.y - o.y,
	}
}

type Direction Position

func Abs(x int) int {
	if x > 0 {
		return x
	} else {
		return -x
	}
}

func (d Direction) Length() int {
	return Abs(d.x) + Abs(d.y)
}

func (d Direction) Normalise() Direction {
	gcd := GCD(Abs(d.x), Abs(d.y))
	return Direction{
		d.x / gcd,
		d.y / gcd,
	}
}

type Dimensions struct {
	width, height int
}

func GCD(x, y int) int {
	if y == 0 {
		if x == 0 {
			return 1
		}
		return x
	}

	return GCD(y, x%y)
}

func Angle(origin, point Position) (Direction, int) {
	dir := point.Sub(origin)
	length := dir.Length()

	return dir.Normalise(), length
}

func read() (Dimensions, []Position) {
	var width, height int

	positions := make([]Position, 0)

	scanner := bufio.NewScanner(os.Stdin)
	for scanner.Scan() {
		line := scanner.Text()
		width = len(line)
		height++

		for x, v := range line {
			if v == '#' {
				positions = append(positions, Position{x, height - 1})
			}
		}
	}

	return Dimensions{width, height}, positions
}

func countReachable(origin Position, asteroids []Position) int {
	directions := make(map[Direction]bool)

	for _, a := range asteroids {
		dir, _ := Angle(origin, a)
		directions[dir] = true
	}

	return len(directions)
}

func main() {
	_, asteroids := read()

	bestReachable := 0

	for _, pos := range asteroids {
		reachable := countReachable(pos, asteroids)
		if reachable > bestReachable {
			bestReachable = reachable
		}
	}

	// Discount the asteroid monitoring station is on
	fmt.Println(bestReachable - 1)
}
