package main

import "fmt"

func fuel(mass int) int {
	return (mass / 3) - 2
}

func main() {
	sum := 0

	for {
		var mass int
		if _, err := fmt.Scanf("%d", &mass); err != nil {
			break
		}
		sum += fuel(mass)
	}

	fmt.Println(sum)
}