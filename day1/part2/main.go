package main

import "fmt"

func fuel(mass int) int {
	f := (mass / 3) - 2
	if f < 0 {
		return 0
	} else {
		return f
	}
}

func main() {
	sum := 0

	for {
		var mass int
		if _, err := fmt.Scanf("%d", &mass); err != nil {
			break
		}
		for mass > 0 {
			mass = fuel(mass)
			sum += mass
		}
	}

	fmt.Println(sum)
}