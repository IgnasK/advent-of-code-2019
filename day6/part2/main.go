package main

import (
	"bufio"
	"fmt"
	"os"
	"strings"
)

type Edge struct {
	from, to string
}

func read() map[string]string {
	edges := make(map[string]string)


	scanner := bufio.NewScanner(os.Stdin)
	for scanner.Scan() {
		line := scanner.Text()

		parts := strings.Split(line, ")")
		edges[parts[1]] = parts[0]
	}
	return edges
}


func orbiters(object string, edges map[string]string) []string {
	orbiters := make([]string, 0)

	for  {
		if parent, ok := edges[object]; ok {
			orbiters = append(orbiters, parent)
			object = parent
		} else {
			break
		}
	}

	return orbiters
}

func drop_common(a, b []string) ([]string, []string) {
	for a[len(a) - 1] == b[len(b) - 1] {
		a = a[:len(a) - 1]
		b = b[:len(b) - 1]
	}

	return a, b
}


func main() {
	edges := read()

	parents_you := orbiters("YOU", edges)
	parents_santa := orbiters("SAN", edges)

	unique_you, unique_santa := drop_common(parents_you, parents_santa)

	fmt.Println(len(unique_you) + len(unique_santa))
}