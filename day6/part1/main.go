package main

import (
	"bufio"
	"fmt"
	"os"
	"strings"
)

type Edge struct {
	from, to string
}

func read() map[string]string {
	edges := make(map[string]string)


	scanner := bufio.NewScanner(os.Stdin)
	for scanner.Scan() {
		line := scanner.Text()

		parts := strings.Split(line, ")")
		edges[parts[1]] = parts[0]
	}
	return edges
}

func countPartialOrbits(object string, edges map[string]string, orbits map[string]int) int {
	if val, ok := orbits[object]; ok {
		return val
	}

	if orbiter, ok := edges[object]; ok {
		val := countPartialOrbits(orbiter, edges, orbits)
		orbits[object] = val + 1
		return val + 1
	}

	return 0
}

func countOrbits(edges map[string]string) int {
	orbits := make(map[string]int)

	sum := 0

	for object := range edges {
		sum += countPartialOrbits(object, edges, orbits)
	}

	return sum
}


func main() {
	edges := read()

	orbits := countOrbits(edges)

	fmt.Println(orbits)
}