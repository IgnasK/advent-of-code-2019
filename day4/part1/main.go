package main

import "fmt"

func Valid(v int) bool {
	var sameDigits bool

	lastDigit := 10
	for v > 0 {
		digit := v % 10
		if digit == lastDigit {
			sameDigits = true
		}
		if digit > lastDigit {
			return false
		}

		lastDigit = digit

		v = v / 10
	}

	return sameDigits
}

func main() {
	var low, high int
	if _, err := fmt.Scanf("%d-%d", &low, &high); err != nil {
		panic(err)
	}

	total := 0
	for i := low; i <= high; i++ {
		if Valid(i) {
			total += 1
		}
	}

	fmt.Println(total)

}