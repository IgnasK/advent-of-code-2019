package main

import "testing"

func TestValid(t *testing.T) {
	check := func(v int, expected bool) {
		if Valid(v) != expected {
			t.Errorf("Valid(%d) should be %v", v, expected)
		}
	}

	check(122345, true)
	check(111111, false)
	check(223450, false)
	check(123789, false)
	check(112233, true)
	check(123444, false)
	check(111122, true)
}