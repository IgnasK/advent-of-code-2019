package main

import "fmt"

func Valid(v int) bool {
	var sameDigits bool
	var streak int

	lastDigit := 10
	for v > 0 {
		digit := v % 10
		if digit != lastDigit {
			if streak == 2 {
				sameDigits = true
			}
			streak = 0
		}
		streak += 1
		if digit > lastDigit {
			return false
		}

		lastDigit = digit

		v = v / 10
	}
	if streak == 2 {
		sameDigits = true
	}

	return sameDigits
}

func main() {
	var low, high int
	if _, err := fmt.Scanf("%d-%d", &low, &high); err != nil {
		panic(err)
	}

	total := 0
	for i := low; i <= high; i++ {
		if Valid(i) {
			total += 1
		}
	}

	fmt.Println(total)

}