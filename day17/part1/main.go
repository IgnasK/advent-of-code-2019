package main

import (
	"bufio"
	"fmt"
	"math/big"
	"os"
	"strings"
)

type ParameterMode int

const (
	Position  ParameterMode = 0
	Immediate ParameterMode = 1
	Relative  ParameterMode = 2
)

type ParameterModes []ParameterMode

func (p ParameterModes) GetMode(pos, length int) ParameterMode {

	if pos >= len(p) {
		return Position
	} else {
		return p[pos]
	}
}

func NewParameterModes(opCode int) ParameterModes {
	opCode /= 100

	modes := make(ParameterModes, 0)

	for opCode > 0 {
		modes = append(modes, ParameterMode(opCode%10))

		opCode /= 10
	}

	return modes
}

type Program struct {
	tape    map[int]*big.Int
	pointer int
	relBase int

	in           chan *big.Int
	out          chan *big.Int
}

func (p Program) Copy() Program {
	newProgram := Program{
		tape:         make(map[int]*big.Int, len(p.tape)),
		pointer:      p.pointer,
		in:           make(chan *big.Int),
		out:          make(chan *big.Int),
	}

	for i, v := range p.tape {
		newProgram.tape[i] = big.NewInt(0).Set(v)
	}

	return newProgram
}

func (p *Program) Run() {
	for p.Step() {
	}
	close(p.out)
	//Consume further input
	for {
		if _, ok := <-p.in; !ok {
			break
		}
	}
}

func (p *Program) Value(relativePos int, mode ParameterMode) *big.Int {
	value := p.tape[p.pointer+relativePos]
	switch mode {
	case Position:
		if p.tape[int(value.Int64())] == nil {
			p.tape[int(value.Int64())] = big.NewInt(0)
		}
		return p.tape[int(value.Int64())]
	case Immediate:
		return value
	case Relative:
		if p.tape[p.relBase+int(value.Int64())] == nil {
			p.tape[p.relBase+int(value.Int64())] = big.NewInt(0)
		}
		return p.tape[p.relBase+int(value.Int64())]
	}

	panic(fmt.Sprintf("Unknown mode %d", mode))
}

func (p *Program) ReadInput() *big.Int {
	value := <-p.in
	return value
}

func (p *Program) WriteOutput(value *big.Int) {
	p.out <- big.NewInt(0).Set(value)
}

func (p *Program) OpCode(relativePos int) (int, ParameterModes) {
	value := int(p.Value(relativePos, Immediate).Int64())
	opCode := value % 100

	return opCode, NewParameterModes(value)
}

func (p *Program) Step() bool {
	//fmt.Printf("%d:\t%s\t%s\n", p.pointer, p.tape[p.pointer].String(), p.tape[p.pointer+1].String())
	switch opcode, modes := p.OpCode(0); opcode {
	case 1:
		a := p.Value(1, modes.GetMode(0, 3))
		b := p.Value(2, modes.GetMode(1, 3))
		p.Value(3, modes.GetMode(2, 3)).Add(a, b)
		p.pointer += 4
	case 2:
		a := p.Value(1, modes.GetMode(0, 3))
		b := p.Value(2, modes.GetMode(1, 3))
		p.Value(3, modes.GetMode(2, 3)).Mul(a, b)
		p.pointer += 4
	case 3:
		p.Value(1, modes.GetMode(0, 1)).Set(p.ReadInput())
		p.pointer += 2
	case 4:
		p.WriteOutput(p.Value(1, modes.GetMode(0, 1)))
		p.pointer += 2
	case 5:
		a := p.Value(1, modes.GetMode(0, 2))
		b := p.Value(2, modes.GetMode(1, 2))
		if a.Cmp(big.NewInt(0)) != 0 {
			p.pointer = int(b.Int64())
		} else {
			p.pointer += 3
		}
	case 6:
		a := p.Value(1, modes.GetMode(0, 2))
		b := p.Value(2, modes.GetMode(1, 2))
		if a.Cmp(big.NewInt(0)) == 0 {
			p.pointer = int(b.Int64())
		} else {
			p.pointer += 3
		}
	case 7:
		a := p.Value(1, modes.GetMode(0, 3))
		b := p.Value(2, modes.GetMode(1, 3))
		if a.Cmp(b) < 0 {
			p.Value(3, modes.GetMode(2, 3)).Set(big.NewInt(1))
		} else {
			p.Value(3, modes.GetMode(2, 3)).Set(big.NewInt(0))
		}
		p.pointer += 4
	case 8:
		a := p.Value(1, modes.GetMode(0, 3))
		b := p.Value(2, modes.GetMode(1, 3))
		if a.Cmp(b) == 0 {
			p.Value(3, modes.GetMode(2, 3)).Set(big.NewInt(1))
		} else {
			p.Value(3, modes.GetMode(2, 3)).Set(big.NewInt(0))
		}
		p.pointer += 4
	case 9:
		a := p.Value(1, modes.GetMode(0, 1))
		p.relBase += int(a.Int64())
		p.pointer += 2
	case 99:
		return false
	default:
		panic(fmt.Sprintf("Unknown opcode: %d", opcode))
	}
	return true
}

func read() *Program {
	tape := make(map[int]*big.Int, 0)

	scanner := bufio.NewScanner(os.Stdin)
	pos := 0
	for scanner.Scan() {
		line := scanner.Text()
		numbers := strings.Split(line, ",")
		for _, value := range numbers {
			if v, ok := big.NewInt(0).SetString(value, 10); ok {
				tape[pos] = v
			} else {
				panic(fmt.Sprintf("%s is not a number", value))
			}
			pos++
		}
	}

	return &Program{
		tape:         tape,
		pointer:      0,
		in:           make(chan *big.Int),
		out:          make(chan *big.Int),
	}
}

type Coords struct {
	x, y int
}

func (c Coords) Add(v Coords) Coords {
	return Coords{c.x + v.x, c.y + v.y}
}

type Map map[Coords]int

func Min(a, b int) int {
	if a < b {
		return a
	}
	return b
}

func Max(a, b int) int {
	if a > b {
		return a
	}
	return b
}

func (m Map) Print() {
	minX, maxX, minY, maxY := 0, 0, 0, 0

	if len(m) == 0 {
		return
	}

	for c := range m {
		minX = Min(minX, c.x)
		maxX = Max(maxX, c.x)
		minY = Min(minY, c.y)
		maxY = Max(maxY, c.y)
	}

	for y := minY; y <= maxY; y++ {
		for x := minX; x <= maxX; x++ {
			v := m[Coords{x, y}]
			fmt.Printf("%c", v)
		}
		fmt.Println()
	}
}

func execute(program *Program) Map {
	m := make(Map)

	go program.Run()

	pos := Coords{}

	for {
		if v, ok := <- program.out; ok {
			value := int(v.Int64())

			if value == '\n' {
				pos.x = 0
				pos.y++
			} else {
				m[pos] = value
				pos.x++
			}
		} else {
			break
		}
	}

	return m
}

var Directions = [...]Coords{
	{0, -1},
	{1, 0},
	{0, 1},
	{-1, 0},
}

func intersections(m Map) []Coords {
	results := make([]Coords, 0)
	for pos, v := range m {
		if v != '#' {
			continue
		}

		ok := true
		for _, dir := range Directions {
			if m[pos.Add(dir)] != '#' {
				ok = false
			}
		}

		if ok {
			results = append(results, pos)
		}

	}

	return results
}


func main() {
	program := read()

	m := execute(program)
	inter := intersections(m)

	res := 0
	for _, pos := range inter {
		m[pos] = 'O'
		res += pos.x * pos.y
	}
	m.Print()
	fmt.Println(res)
}
