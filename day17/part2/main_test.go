package main

import (
	"fmt"
	"testing"
)

func TestEncodeOrder(t *testing.T) {
	steps := []string{"R", "6", "L", "12", "R", "6", "R", "6", "L", "12", "R", "6", "L", "12", "R", "6", "L", "8", "L", "12", "R", "12", "L", "10", "L", "10", "L", "12", "R", "6", "L", "8", "L", "12", "R", "12", "L", "10", "L", "10", "L", "12", "R", "6", "L", "8", "L", "12", "R", "12", "L", "10", "L", "10", "L", "12", "R", "6", "L", "8", "L", "12", "R", "6", "L", "12", "R", "6"}
	templates := [][]string{
		{"R", "6", "L", "12", "R", "6"},
		{"L", "12", "R", "6", "L", "8", "L", "12"},
		{"R", "12", "L", "10", "L", "10"},
	}

	expected := []int{0, 0, 1, 2, 1, 2, 1, 2, 1, 0}

	result, ok := EncodeOrder(steps, templates, 21)
	fmt.Println(result, ok)

	for i, e := range expected {
		if result[i] != e {
			t.Errorf("Expected %d, found %d at pos %d", e, result[i], i)
		}
	}
}
