package main

import (
	"bufio"
	"fmt"
	"os"
	"strings"
)

type Element struct {
	count int
	chemical string
}

func readElement(str string) Element {
	var e Element

	if _, err := fmt.Sscanf(str,"%d %s", &e.count, &e.chemical); err != nil {
		panic(err)
	}

	return e
}

type Reaction struct {
	resultCount int
	inputs []Element
}

func (r Reaction) RequiredCounts(resultCount int) map[string]int {
	counts := make(map[string]int)

	// Can generate infinite amount without inputs
	if len(r.inputs) == 0 {
		return counts
	}

	requiredReactions := (resultCount + r.resultCount - 1) / (r.resultCount)

	for _, element := range r.inputs {
		counts[element.chemical] += element.count * requiredReactions
	}

	return counts
}

type Reactions map[string]Reaction

func read() Reactions {
	reactions := make(Reactions)

	scanner := bufio.NewScanner(os.Stdin)
	for scanner.Scan() {
		line := strings.Split(scanner.Text(), " => ")
		result := readElement(line[1])
		elementsString := strings.Split(line[0], ", ")
		elements := make([]Element, 0)
		for _, el := range elementsString {
			elements = append(elements, readElement(el))
		}

		reactions[result.chemical] = Reaction{result.count, elements}
	}

	return reactions
}

func sortRecursive(chemical string, reactions Reactions, order *[]string, visited *map[string]bool) {
	if (*visited)[chemical] {
		return
	}
	(*visited)[chemical] = true

	for _, input := range reactions[chemical].inputs {
		sortRecursive(input.chemical, reactions, order, visited)
	}

	*order = append(*order, chemical)
}

func sort(reactions Reactions) []string {
	order := make([]string, 0)
	visited := make(map[string]bool)

	for chemical := range reactions {
		sortRecursive(chemical, reactions, &order, &visited)
	}

	return order
}

func counts(reactions Reactions, element Element) map[string]int {
	order := sort(reactions)
	counts := make(map[string]int)

	counts[element.chemical] = element.count

	for i := len(order)-1; i >= 0; i-- {
		chemical := order[i]
		reaction := reactions[chemical]

		inputCounts := reaction.RequiredCounts(counts[chemical])
		for c, v := range inputCounts {
			counts[c] += v
		}
	}

	return counts
}


func main()  {
	reactions := read()

	counts := counts(reactions, Element{1, "FUEL"})
	fmt.Println(counts["ORE"])
}