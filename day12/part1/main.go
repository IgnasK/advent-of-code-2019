package main

import "fmt"
import "bufio"
import "os"

type Position struct {
	x, y, z int
}

func (p Position) Add(v Velocity) Position {
	return Position {
		p.x + v.x,
		p.y + v.y,
		p.z + v.z,
	}
}

type Velocity Position

func (v Velocity) Add(other Velocity) Velocity {
	return Velocity((Position(v)).Add(other))
}

func Sign(v int) int {
	if v < 0 {
		return -1
	}
	if v > 0 {
		return 1
	}
	return 0
}

func (p Moon) VelocityDelta(others []Moon) Velocity {
	var dv Velocity

	for _, o := range others {
		dv.x += Sign(o.pos.x - p.pos.x)
		dv.y += Sign(o.pos.y - p.pos.y)
		dv.z += Sign(o.pos.z - p.pos.z)
	}

	return dv
}

func Step(moons []Moon) {
	for i, m := range moons {
		delta := m.VelocityDelta(moons)
		moons[i].velocity = moons[i].velocity.Add(delta)
	}

	for i, m := range moons {
		moons[i].pos = moons[i].pos.Add(m.velocity)
	}
}

type Moon struct {
	pos Position
	velocity Velocity
}

func Abs(x int) int {
	if x > 0 {
		return x
	}
	return -x
}

func (m Moon) Energy() int {
	potential := Abs(m.pos.x) + Abs(m.pos.y) + Abs(m.pos.z)
	kinetic := Abs(m.velocity.x) + Abs(m.velocity.y) + Abs(m.velocity.z)

	return potential * kinetic
}

func (m Moon) Print() {
	fmt.Printf("pos=<x=%d, y=%d, z=%d>, vel=<x=%d, y=%d, z=%d>\n", m.pos.x, m.pos.y, m.pos.z, m.velocity.x, m.velocity.y, m.velocity.z)
}

func read() []Moon {
	moons := make([]Moon, 0)

	scanner := bufio.NewScanner(os.Stdin)
	for scanner.Scan() {
		var x, y, z int
		if _, err := fmt.Sscanf(scanner.Text(), "<x=%d, y=%d, z=%d>", &x, &y, &z); err != nil {
			//panic(err)
			break
		}

		moons = append(moons, Moon{
			pos: Position{
				x, y, z,
			},
			velocity: Velocity{
				0, 0, 0,
			},
		})
	}

	return moons
}

func main() {
	moons := read()

	for i := 0; i < 1000; i++ {
		Step(moons)
	}

	energy := 0
	for _, m := range moons {
		m.Print()
		energy += m.Energy()
	}
	fmt.Println(energy)
}