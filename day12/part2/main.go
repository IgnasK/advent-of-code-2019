package main

import "fmt"
import "bufio"
import "os"

type Position struct {
	x, y, z int
}

func (p Position) Add(v Velocity) Position {
	return Position {
		p.x + v.x,
		p.y + v.y,
		p.z + v.z,
	}
}

type Velocity Position

func (v Velocity) Add(other Velocity) Velocity {
	return Velocity((Position(v)).Add(other))
}

func Sign(v int) int {
	if v < 0 {
		return -1
	}
	if v > 0 {
		return 1
	}
	return 0
}

func (p Moon) VelocityDelta(others []Moon) Velocity {
	var dv Velocity

	for _, o := range others {
		dv.x += Sign(o.pos.x - p.pos.x)
		dv.y += Sign(o.pos.y - p.pos.y)
		dv.z += Sign(o.pos.z - p.pos.z)
	}

	return dv
}

func Step(moons []Moon) {
	for i, m := range moons {
		delta := m.VelocityDelta(moons)
		moons[i].velocity = moons[i].velocity.Add(delta)
	}

	for i, m := range moons {
		moons[i].pos = moons[i].pos.Add(m.velocity)
	}
}

type Moon struct {
	pos Position
	velocity Velocity
}

func Abs(x int) int {
	if x > 0 {
		return x
	}
	return -x
}

func (m Moon) Energy() int {
	potential := Abs(m.pos.x) + Abs(m.pos.y) + Abs(m.pos.z)
	kinetic := Abs(m.velocity.x) + Abs(m.velocity.y) + Abs(m.velocity.z)

	return potential * kinetic
}

func (m Moon) Print() {
	fmt.Printf("pos=<x=%d, y=%d, z=%d>, vel=<x=%d, y=%d, z=%d>\n", m.pos.x, m.pos.y, m.pos.z, m.velocity.x, m.velocity.y, m.velocity.z)
}

func read() []Moon {
	moons := make([]Moon, 0)

	scanner := bufio.NewScanner(os.Stdin)
	for scanner.Scan() {
		var x, y, z int
		if _, err := fmt.Sscanf(scanner.Text(), "<x=%d, y=%d, z=%d>", &x, &y, &z); err != nil {
			//panic(err)
			break
		}

		moons = append(moons, Moon{
			pos: Position{
				x, y, z,
			},
			velocity: Velocity{
				0, 0, 0,
			},
		})
	}

	return moons
}

func FindCycles(moons []Moon) []int {
	seenX := make(map[[8]int]int)
	seenY := make(map[[8]int]int)
	seenZ := make(map[[8]int]int)

	cycleX := 0
	cycleY := 0
	cycleZ := 0

	time := 0
	for cycleX == 0 || cycleY == 0 || cycleZ == 0 {
		curX, curY, curZ := Transpose(moons)

		if _, ok := seenX[curX]; cycleX == 0 && ok {
			cycleX = time
		}
		if _, ok := seenY[curY]; cycleY == 0 && ok {
			cycleY = time
		}
		if _, ok := seenZ[curZ]; cycleZ == 0 && ok {
			cycleZ = time
		}

		seenX[curX] = time
		seenY[curY] = time
		seenZ[curZ] = time

		Step(moons)
		time ++
	}

	return []int{
		cycleX, cycleY, cycleZ,
	}
}

func GCD(x, y int) int {
	if y == 0 {
		if x == 0 {
			return 1
		}
		return x
	}

	return GCD(y, x%y)
}

func LCM(x, y int) int {
	return x/GCD(x, y) * y
}

func Transpose(moons []Moon) ([8]int, [8]int, [8]int) {
	x := [8]int{}
	y := [8]int{}
	z := [8]int{}

	for i, m := range moons {
		x[2*i] = m.pos.x
		x[2*i+1] = m.velocity.x
		y[2*i] = m.pos.y
		y[2*i+1] = m.velocity.y
		z[2*i] = m.pos.z
		z[2*i+1] = m.velocity.z
	}

	return x, y, z
}

func LCMArray(values []int) int {
	lcm := 1
	for _, v := range values {
		lcm = LCM(lcm, v)
	}

	return lcm
}

func main() {
	moons := read()

	cycles := FindCycles(moons)

	fmt.Println(LCMArray(cycles))
}