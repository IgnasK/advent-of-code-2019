package main

import "fmt"

func Abs(x int) int {
	if x > 0 {
		return x
	}
	return -x
}

func read() []int {
	seq := make([]int, 0)
	var line string
	if _, err := fmt.Scanln(&line); err != nil {
		panic(err)
	}

	for _, r := range line {
		seq = append(seq, int(r - '0'))
	}

	res := make([]int, len(seq) * 10000)
	for i := 0; i < len(res); i++ {
		res[i] = seq[i % len(seq)]
	}

	return res
}



// This only works if offset > len(values)/2, in which case the magic values are:
// 0, 0, 0, ..., 1, 1, 1, ..
func Step(values []int) []int {

	for j := len(values)-1; j > 0; j-- {
		values[j-1] += values[j]
		values[j-1] = Abs(values[j-1]) % 10
	}

	return values
}

func PrintSeq(values []int, offset, length int) {
	for i := 0; i < length; i++ {
		fmt.Printf("%d", values[offset+i])
	}
	fmt.Println()
}

func ToNumber(values []int, offset, length int) int {
	res := 0
	for i := 0; i < length; i++ {
		res *= 10
		res += values[offset+i]
	}

	return res
}

func main() {
	seq := read()

	offset := ToNumber(seq, 0, 7)
	seq = seq[offset:]

	for i := 0; i < 100; i++ {
		seq = Step(seq)
	}

	PrintSeq(seq, 0, 8)
}