package main

import "fmt"

type Matrix [][]int

func (m Matrix) Copy() Matrix {
	n := make(Matrix, len(m))
	for i, v := range m {
		n[i] = make([]int, len(v))
		copy(n[i], v)
	}

	return n
}

func (m Matrix) Multiply(n Matrix) Matrix {
	result := make(Matrix, len(m))
	for i := range m {
		result[i] = make([]int, len(n[0]))

		for j := 0; j < len(n[0]); j++ {
			for k := 0; k < len(n); k++ {
				result[i][j] += m[i][k] * n[k][j]
			}
		}
	}

	return result
}

func Power(m Matrix, power int) Matrix {
	if power == 1 {
		return m
	}

	square := m.Multiply(m)
	res := Power(square, power / 2)
	if power % 2 == 0 {
		return res
	} else {
		return res.Multiply(m)
	}
}

func Abs(x int) int {
	if x > 0 {
		return x
	}
	return -x
}

func (m Matrix) Normalize() {
	for i, row := range m {
		for j := range row {
			m[i][j] = Abs(m[i][j]) % 10
		}
	}
}

func Pattern(element, index int) int {
	template := []int{0, 1, 0, -1}

	pos := (index + 1) / (element + 1) % 4

	return template[pos]
}

func PatternMatrix(size int) Matrix {
	m := make(Matrix, size)

	for i := 0; i < size; i++ {
		m[i] = make([]int, size)
		for j := 0; j < size; j++ {
			m[i][j] = Pattern(j, i)
		}
	}

	return m
}

func read() []int {
	seq := make([]int, 0)
	var line string
	if _, err := fmt.Scanln(&line); err != nil {
		panic(err)
	}

	for _, r := range line {
		seq = append(seq, int(r - '0'))
	}

	return seq
}

func Step(values []int, steps int) []int {
	matrix := PatternMatrix(len(values))
	power := Power(matrix, steps)

	res := Matrix{values}.Multiply(power)
	res.Normalize()

	return res[0]
}

func PrintSeq(values []int, length int) {
	for i := 0; i < length && i < len(values); i++ {
		fmt.Printf("%d", values[i])
	}
	fmt.Println()
}

func main() {
	seq := read()
	
	for i := 0; i < 100; i++ {
		seq = Step(seq, 1)
	}

	PrintSeq(seq, 8)
}