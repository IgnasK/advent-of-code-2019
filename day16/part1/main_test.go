package main

import (
	"testing"
)

func TestPattern(t *testing.T) {
	values := []int{0, 1, 1, 0, 0, -1, -1, 0, 0, 1, 1, 0, 0, -1, -1, 0}

	for i, v := range values {
		if p := Pattern(1, i); p != v {
			t.Errorf("Expected %d, got %d at position %d", v, p, i)
		}
	}
}

func TestMultiply(t *testing.T) {
	a := Matrix{
		{1, 2, 3},
		{4, 5, 6},
	}
	b := Matrix{
		{1, 2},
		{3, 4},
		{5, 6},
	}

	expected := Matrix{
		{22, 28},
		{49, 64},
	}

	res := a.Multiply(b)

	for i, row := range expected {
		for j, val := range row {
			if res[i][j] != val {
				t.Errorf("[%d][%d] = %d != %d (expected)", i, j, res[i][j], val)
			}
		}
	}
}

func TestPower(t *testing.T) {
	a := Matrix{
		{1, 1},
		{1, 0},
	}

	expected := Matrix{
		{21, 13},
		{13, 8},
	}

	res := Power(a, 7)

	for i, row := range expected {
		for j, val := range row {
			if res[i][j] != val {
				t.Errorf("[%d][%d] = %d != %d (expected)", i, j, res[i][j], val)
			}
		}
	}
}