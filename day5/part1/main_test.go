package main

import "testing"

func TestParameterModes(t *testing.T) {
	modes := NewParameterModes(1002)

	expectedModes := []ParameterMode{
		Position,
		Immediate,
		Position,
	}

	for i, expected := range expectedModes {
		if actual := modes.GetMode(i, len(expectedModes)); actual != expected {
			t.Errorf("Expected %v mode at position %d, found %v", expected, i, actual)
		}
	}
}

func TestParameterModes2(t *testing.T) {
	modes := NewParameterModes(1102)

	expectedModes := []ParameterMode{
		Immediate,
		Immediate,
		Position,
	}

	for i, expected := range expectedModes {
		if actual := modes.GetMode(i, len(expectedModes)); actual != expected {
			t.Errorf("Expected %v mode at position %d, found %v", expected, i, actual)
		}
	}
}