package main

import (
	"bufio"
	"fmt"
	"os"
	"strings"
)

type Command struct {
	direction rune
	distance int
}

type Segment struct {
	left, top, right, bottom int
}

func (s Segment) surrounds(o Segment) bool {
	 if (s.left == s.right) == (o.left == o.right) {
	 	return false // Two parallel lines
	 }
	 return s.left <= o.left && s.right >= o.right || s.top >= o.top && s.bottom <= o.bottom
}

func (s Segment) Intersects(o Segment) (bool, int, int) {
	if s.surrounds(o) && o.surrounds(s) {
		if s.left == s.right {
			return true, s.left, o.top
		} else {
			return true, o.left, s.top
		}
	}
	return false, 0, 0
}

func Abs(v int) int {
	if v > 0 {
		return v
	}
	return -v
}

func Manhattan(x, y int) int {
	return Abs(x) + Abs(y)
}

func NewSegment(x, y, x2, y2 int) Segment {
	if x < x2 {
		return Segment{x, y, x2, y2}
	} else if y > y2 {
		return Segment{x, y, x2, y2}
	} else {
		return Segment{x2, y2, x, y}
	}
}

func MovePoint(x, y int, command Command) (int, int) {
	switch command.direction {
	case 'U':
		return x, y + command.distance
	case 'R':
		return x + command.distance, y
	case 'D':
		return x, y - command.distance
	case 'L':
		return x - command.distance, y
	}
	return x, y
}

type Line []Segment

func read() (Line, Line) {
	lines := make([]Line, 0)
	scanner := bufio.NewScanner(os.Stdin)
	for scanner.Scan() {
		line := scanner.Text()
		enc_commands := strings.Split(line, ",")
		segments := make([]Segment, 0)
		var x, y int
		for _, c := range enc_commands {
			var direction rune
			var distance int

			if _, err := fmt.Sscanf(c, "%c%d", &direction, &distance); err != nil {
				panic(fmt.Sprintf("Invalid command %s (%s)", c, err))
			}
			newX, newY := MovePoint(x, y, Command{direction, distance})
			segments = append(segments, NewSegment(x, y, newX, newY))
			x = newX
			y = newY
		}
		lines = append(lines, segments)
	}

	return lines[0], lines[1]
}

func main() {
	lineA, lineB := read()
	lowest := 1000000000
	for _, segmentA := range lineA {
		for _, segmentB := range lineB {
			if intersects, x, y := segmentA.Intersects(segmentB); intersects {
				distance := Manhattan(x, y)
				fmt.Printf("Found intersection at (%d, %d), distance %d\n", x, y, distance)
				if distance < lowest {
					lowest = distance
				}
			}
		}
	}
	fmt.Printf("Lowest distance: %d\n", lowest)
}