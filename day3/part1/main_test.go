package main

import "testing"

func TestSegment_Intersects(t *testing.T) {
	a := NewSegment(0, 0, 0, 300)
	b := NewSegment(-10, 100, 300, 100)

	intersects, x, y := a.Intersects(b)
	if !intersects {
		t.Errorf("%v should intersect %v", a, b)
	}
	if x != 0 || y != 100 {
		t.Errorf("Interection point should be (0, 100), got (%d, %d)", x, y)
	}
}

func TestSegment_Intersects_failure(t *testing.T) {
	a := NewSegment(0, 0, 0, 300)
	b := NewSegment(-10, 400, 300, 400)

	intersects, _, _ := a.Intersects(b)
	if intersects {
		t.Errorf("%v should not intersect %v", a, b)
	}
}
