package main

import "testing"

func TestNextPermutation(t *testing.T) {
	perm := []int{ 1, 2, 3, 4 }

	expectedPermutations := [][]int {
		{1, 2, 4, 3},
		{1, 3, 2, 4},
		{1, 3, 4, 2},
		{1, 4, 2, 3},
		{1, 4, 3, 2},
		{2, 1, 3, 4},
		{2, 1, 4, 3},
	}

	for _, expectedPermutation := range expectedPermutations {

		if ok := NextPermutation(perm); !ok {
			t.Errorf("%v should not be the first permutation", ok)
		}

		for i := range perm {
			if perm[i] != expectedPermutation[i] {
				t.Errorf("Next permutation: %v, should be %v", perm, expectedPermutation)
				break
			}
		}
	}
}

func TestTotalPermutations(t *testing.T) {
	perm := []int{1, 2, 3, 4}

	total := 24
	count := 1

	for NextPermutation(perm) {
		count += 1
	}

	if count != total {
		t.Errorf("Expected %d permutations, got %d, final permutation %v", total, count, perm)
	}
}

