package main

import (
	"bufio"
	"fmt"
	"os"
	"strconv"
	"strings"
)

type ParameterMode int

const (
	Position ParameterMode = 0
	Immediate ParameterMode = 1
)

type ParameterModes []ParameterMode

func (p ParameterModes) GetMode(pos, length int) ParameterMode {

	if pos >= len(p) {
		return Position
	} else {
		return p[pos]
	}
}

func NewParameterModes(opCode int) ParameterModes {
	opCode /= 100

	modes := make(ParameterModes, 0)

	for opCode > 0 {
		modes = append(modes, ParameterMode(opCode % 10))

		opCode /= 10
	}

	return modes
}

type Program struct {
	tape []int
	pointer int

	in chan int
	out chan int
	done chan bool
}

func (p Program) Copy() Program {
	newProgram := Program{
		tape:      make([]int, len(p.tape)),
		pointer:   p.pointer,
		in:        make(chan int),
		out:       make(chan int),
	}

	copy(newProgram.tape, p.tape)

	return newProgram
}

func (p *Program) Run() {
	for p.Step() {}
	close(p.out)
	//Consume further input
	for {
		if _, ok := <-p.in; !ok {
			break
		}
	}
}

func (p *Program) Value(relativePos int, mode ParameterMode) int {
	value := p.tape[p.pointer+relativePos]
	if mode == Position {
		return p.tape[value]
	} else {
		return value
	}
}

func (p *Program) SetValue(relativePos int, value int) {
	p.tape[p.tape[p.pointer+relativePos]] = value
}

func (p *Program) ReadInput() int {
	value := <-p.in
	return value
}

func (p *Program) WriteOutput(value int) {
	p.out <- value
}

func (p *Program) OpCode(relativePos int) (int, ParameterModes) {
	value := p.Value(relativePos, Immediate)
	opCode := value % 100

	return opCode, NewParameterModes(value)
}

func (p *Program) Step() bool {
	//fmt.Printf("%d:\t%d\n", p.pointer, p.tape[p.pointer])
	switch opcode, modes := p.OpCode(0); opcode {
	case 1:
		a := p.Value(1, modes.GetMode(0, 3))
		b := p.Value(2, modes.GetMode(1, 3))
		p.SetValue(3, a + b)
		p.pointer += 4
	case 2:
		a := p.Value(1, modes.GetMode(0, 3))
		b := p.Value(2, modes.GetMode(1, 3))
		p.SetValue(3, a * b)
		p.pointer += 4
	case 3:
		p.SetValue(1, p.ReadInput())
		p.pointer += 2
	case 4:
		p.WriteOutput(p.Value(1, modes.GetMode(0, 1)))
		p.pointer += 2
	case 5:
		a := p.Value(1, modes.GetMode(0, 2))
		b := p.Value(2, modes.GetMode(1, 2))
		if a != 0 {
			p.pointer = b
		} else {
			p.pointer += 3
		}
	case 6:
		a := p.Value(1, modes.GetMode(0, 2))
		b := p.Value(2, modes.GetMode(1, 2))
		if a == 0 {
			p.pointer = b
		} else {
			p.pointer += 3
		}
	case 7:
		a := p.Value(1, modes.GetMode(0, 2))
		b := p.Value(2, modes.GetMode(1, 2))
		if a < b {
			p.SetValue(3, 1)
		} else {
			p.SetValue(3, 0)
		}
		p.pointer += 4
	case 8:
		a := p.Value(1, modes.GetMode(0, 2))
		b := p.Value(2, modes.GetMode(1, 2))
		if a == b {
			p.SetValue(3, 1)
		} else {
			p.SetValue(3, 0)
		}
		p.pointer += 4
	case 99:
		return false
	default:
		panic(fmt.Sprintf("Unknown opcode: %d", opcode))
	}
	return true
}

func read() *Program {
	tape := make([]int, 0)


	scanner := bufio.NewScanner(os.Stdin)
	for scanner.Scan() {
		line := scanner.Text()
		numbers := strings.Split(line, ",")
		for _, value := range numbers {
			if v, err := strconv.Atoi(value); err == nil {
				tape = append(tape, v)
			} else {
				panic(fmt.Sprintf("%s is not a number", value))
			}
		}
	}

	return &Program{
		tape:    tape,
		pointer: 0,
		in: make(chan int),
		out: make(chan int),
	}
}

func testOrder(template Program, order []int) int {
	signal := 0

	programs := make([]Program, 0)
	for _, v := range order {
		program := template.Copy()
		programs = append(programs, program)
		go program.Run()

		program.in <- v
	}

	finished := false
	for !finished {
		for _, program := range programs {
			program.in <- signal

			select {
			case s, open := <-program.out:
				if !open {
					finished = true
					close(program.in)
				} else {
					signal = s
				}
			}
		}
	}

	return signal
}

func testPermutations(template Program) int {
	permutation := make([]int, 5)
	for i := 0; i < 5; i++ {
		permutation[i] = i + 5
	}

	maximum := 0

	for {
		value := testOrder(template, permutation)
		if value > maximum {
			maximum = value
		}

		if !NextPermutation(permutation) {
			break
		}
	}

	return maximum
}

func main() {
	program := read()

	result := testPermutations(*program)

	fmt.Println(result)
}
