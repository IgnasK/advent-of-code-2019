package main

func NextPermutation(values []int) bool {
	if len(values) <= 1 {
		return false
	}

	k := len(values) - 2
	for ; k > -1 && values[k] >= values[k+1]; k -= 1 {}
	l := len(values) - 1

	var nextPerm bool

	if k >= 0 {
		for ; values[l] <= values[k]; l -= 1 {}
		values[l], values[k] = values[k], values[l]

		nextPerm = true
	} else {
		nextPerm = false
	}

	for i, j := k+1, len(values)-1; i < j; i, j = i+1, j-1 {
		values[i], values[j] = values[j], values[i]
	}

	return nextPerm
}