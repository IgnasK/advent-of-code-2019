package main

import (
	"bufio"
	"fmt"
	"os"
	"strconv"
	"strings"
)

type Program struct {
	tape []int
	pointer int
}

func (p *Program) Reset(original_tape []int) {
	copy(p.tape, original_tape)
	p.pointer = 0
}

func (p *Program) Value(relativePos int) int {
	return p.tape[p.pointer + relativePos]
}

func (p *Program) Step() bool {
	switch opcode := p.Value(0); opcode {
	case 1:
		a := p.Value(1)
		b := p.Value(2)
		dest := p.Value(3)
		p.tape[dest] = p.tape[a] + p.tape[b]
		p.pointer += 4
	case 2:
		a := p.Value(1)
		b := p.Value(2)
		dest := p.Value(3)
		p.tape[dest] = p.tape[a] * p.tape[b]
		p.pointer += 4
	case 99:
		return false
	default:
		panic(fmt.Sprintf("Unknown opcode: %d", opcode))
	}
	return true
}

func read() *Program {
	tape := make([]int, 0)


	scanner := bufio.NewScanner(os.Stdin)
	for scanner.Scan() {
		line := scanner.Text()
		numbers := strings.Split(line, ",")
		for _, value := range numbers {
			if v, err := strconv.Atoi(value); err == nil {
				tape = append(tape, v)
			} else {
				panic(fmt.Sprintf("%s is not a number", value))
			}
		}
	}

	return &Program{
		tape:    tape,
		pointer: 0,
	}
}

func main() {
	target := 19690720
	program := read()
	original_tape := make([]int, len(program.tape))
	copy(original_tape, program.tape)

	for noun := 1; noun < 100; noun += 1 {
		for verb := 1; verb < 100; verb += 1 {
			fmt.Printf("Checking %02d%02d\n", noun, verb)
			program.Reset(original_tape)
			program.tape[1] = noun
			program.tape[2] = verb
			for program.Step() {}
			if program.tape[0] == target {
				fmt.Printf("Found value %02d%02d\n", noun, verb)
				return
			}
		}
	}

}
