package main

import (
	"bufio"
	"fmt"
	"os"
	"strconv"
	"strings"
)

type Program struct {
	tape []int
	pointer int
}

func (p *Program) Value(relativePos int) int {
	return p.tape[p.pointer + relativePos]
}

func (p *Program) Step() bool {
	switch opcode := p.Value(0); opcode {
	case 1:
		a := p.Value(1)
		b := p.Value(2)
		dest := p.Value(3)
		p.tape[dest] = p.tape[a] + p.tape[b]
		p.pointer += 4
	case 2:
		a := p.Value(1)
		b := p.Value(2)
		dest := p.Value(3)
		p.tape[dest] = p.tape[a] * p.tape[b]
		p.pointer += 4
	case 99:
		return false
	default:
		panic(fmt.Sprintf("Unknown opcode: %d", opcode))
	}
	return true
}

func read() *Program {
	tape := make([]int, 0)


	scanner := bufio.NewScanner(os.Stdin)
	for scanner.Scan() {
		line := scanner.Text()
		numbers := strings.Split(line, ",")
		for _, value := range numbers {
			if v, err := strconv.Atoi(value); err == nil {
				tape = append(tape, v)
			} else {
				panic(fmt.Sprintf("%s is not a number", value))
			}
		}
	}

	return &Program{
		tape:    tape,
		pointer: 0,
	}
}

func main() {
	program := read()

	program.tape[1] = 12
	program.tape[2] = 2
	for program.Step() {}
	fmt.Println(program.tape[0])
}
